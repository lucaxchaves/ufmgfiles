#include <stdio.h> 
#include <stdlib.h>
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <unistd.h> 
#include <string.h> 
#include <errno.h>

/// PAssar a senha via cargs

#define PORT 51511
#define DEBUG


void enviarString(int socketFd, char* valor);
void enviarInteiro(int socketFd, int valor);

void lerValorEsperado(int socketFd, char* valorEsperado);

void encerrarConexao(int socketFd);
void encerrarPorErro(int socketFd, char* mensagem);
    
int main(int argc, char const *argv[]) 
{ 
    int sock = 0; 
    struct sockaddr_in serv_addr; 

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) 
    { 
        printf("\n Socket creation error \n"); 
        return -1; 
    } 
   
    serv_addr.sin_family = AF_INET; 
    serv_addr.sin_port = htons(PORT); 
       
    // Convert IPv4 and IPv6 addresses from text to binary form 
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0)  
    { 
        encerrarPorErro(sock, "Endereço não suportado");
    } 
   
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) 
    { 
        encerrarPorErro(sock, "Falha ao conectar");
    } 
    
    
    lerValorEsperado(sock, "READY");

    //Envia a senha
    enviarString(sock, "4321");



    enviarString(sock, "OK");
    encerrarConexao(sock);

    return 0; 
} 

void encerrarConexao(int socketFd){
    close(socketFd);
}

void lerValorEsperado(int socketFd, char* valorEsperado){
    char* buffer = NULL;
    
    int bufferLen = (int) strlen(valorEsperado);
    buffer = (char*) malloc(sizeof(char) * bufferLen);
    buffer[0] = '\0';

    if(recv(socketFd,buffer, bufferLen, 0) < 0){
        encerrarPorErro(socketFd, strerror(errno));
    }

    if(strcmp(buffer, valorEsperado) != 0){
        encerrarPorErro(socketFd, "o valor esperado não foi recebido");
    }

    free(buffer);    
}

void encerrarPorErro(int sockFd, char* mensagem){
    #ifdef DEBUG
    fprintf(stderr, "[ERRO] %s\n", mensagem);
    #endif
    encerrarConexao(sockFd);
    exit(EXIT_FAILURE);
}


void enviarString(int socketFd, char* valor){
    if(send(socketFd, valor, strlen(valor), 0) < 0){
        encerrarPorErro(socketFd, strerror(errno));
    }
}
void enviarInteiro(int socketFd, int valor){
    int valorRede = htonl(valor);
    if(send(socketFd,&valorRede, sizeof(valorRede),0) < 0){
        encerrarPorErro(socketFd, strerror(errno));
    }
}