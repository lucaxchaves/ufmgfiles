#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <errno.h>


struct List
{
    int count;
    struct ListItem *first;
    struct ListItem *last;
};

struct ListItem
{
    int id;
    struct ListItem *next;
};

#define List struct List
#define ListItem struct ListItem

List *criarLista();

int registraPresenca(List *list, int id);

int destruirLista(List *list);


void processarRequisicao(int sock, List *list);
void processarRequisicaoAluno(int sock, List *list);
void processarRequisicaoProfessor(int sock, List *list);

void enviarString(int sock, char* valor);
void enviarInteiro(int sock, int valor);
void lerValorEsperado(int sock, char* valorEsperado);
int lerInteiro(int sock);


void encerrarConexao(int sock);
void encerrarPorErro(int sock, char* mensagem);

#define SENHA_ALUNO "1234"
#define SENHA_PROFESSOR "4567"
#define PORT 51511
#define REQUEST_COUNT 1



//Define como modo de debug, exibindo mensagens de debug.
//Para desabilitar basta comentar a linha abaixo:
#define DEBUG



int main(int argc, char *argv[]) 
{ 
    int serverSocket, socketClient; 
    struct sockaddr_in address; 
    int opt = 1; 
    int addrlen = sizeof(address); 
    
    if ((serverSocket = socket(AF_INET, SOCK_STREAM, 0)) == 0) 
    { 
        
        exit(EXIT_FAILURE); 
    } 
       
    //Força ao inicio na mesma porta e Ip
    if (setsockopt(serverSocket, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, 
                                                  &opt, sizeof(opt))) 
    { 
        perror("setsockopt"); 
        exit(EXIT_FAILURE); 
    } 
    address.sin_family = AF_INET; 
    address.sin_addr.s_addr = INADDR_ANY; 
    address.sin_port = htons( PORT ); 
       
    if (bind(serverSocket, (struct sockaddr *)&address,  
                                 sizeof(address))<0) 
    { 
        encerrarPorErro(serverSocket, strerror(errno));
    } 
    if (listen(serverSocket, 3) < 0) 
    { 
        encerrarPorErro(serverSocket, strerror(errno));
    } 
    
    List* list = criarLista();
    while(1){
        if ((socketClient = accept(serverSocket, (struct sockaddr *)&address,  
                       (socklen_t*)&addrlen))<0) 
        { 
            perror("accept"); 
            exit(EXIT_FAILURE); 
        }    
        processarRequisicao(socketClient, list);
        encerrarConexao(socketClient);
    }
    encerrarConexao(serverSocket);
    return 0; 
} 


void processarRequisicao(int socketHandle, List *list)
{
    enviarString(socketHandle, "READY");

    char bufferSenha[5] = {0};    
    
    if(recv(socketHandle, bufferSenha, 4,0) <  0){
        printf("erro ao receber a senha\n");
        exit(EXIT_FAILURE);
    }

    if (strcmp(SENHA_ALUNO, bufferSenha) == 0)
    {
        processarRequisicaoAluno(socketHandle, list);
    }
    else if (strcmp(SENHA_PROFESSOR, bufferSenha) == 0)
    {
        processarRequisicaoProfessor(socketHandle, list);
    }

}

void processarRequisicaoAluno(int sock, List *list)
{
    enviarString(sock, "OK");
    enviarString(sock, "MATRICULA");
    int numeroMatricula  = lerInteiro(sock);
    registraPresenca(list,  numeroMatricula);
}

void processarRequisicaoProfessor(int sock, List *list)
{
    ListItem *item = list->first;

    while (item != NULL)
    {
        int id = item->id;
        id = htonl(id);

       // send(sock, id, 4,0);

        if (item->next == NULL)
        {
            send(sock, "\n", 1, 0);
        }
        item = item->next;
    }
}

List* criarLista()
{
    List *list = (List *)malloc(sizeof(List));
    list->count = 0;
    list->first = NULL;
    list->last = NULL;
    return list;
}

int registraPresenca(List *list, int id)
{
    if (list == NULL)
        return EXIT_FAILURE;

    printf("Nova matricula %d", id);

    ListItem *novoItem = (ListItem *)malloc(sizeof(ListItem));

    novoItem->id = id;
    novoItem->next = NULL;

    if (list->first == NULL)
    {
        list->first = novoItem;
    }
    else
    {
        list->last->next = novoItem;
    }

    list->last = novoItem;

    list->count++;
    novoItem = NULL;
    return EXIT_SUCCESS;
}

int destruirLIsta(List *list)
{
    if (list == NULL)
        return EXIT_SUCCESS;

    ListItem *item = list->first;
    while (item != NULL)
    {
        ListItem *nextPtr = item->next;
        free(item);
        item = nextPtr;
    }
    item = NULL;
    list->first = NULL;
    list->last = NULL;
    free(list);

    list = NULL;

    return EXIT_SUCCESS;
}

void encerrarConexao(int sock){
    shutdown(sock, SHUT_RDWR);
    close(sock);
}

void lerValorEsperado(int sock, char* valorEsperado){
    char* buffer = NULL;
    
    int bufferLen = (int) strlen(valorEsperado);
    buffer = (char*) malloc(sizeof(char) * bufferLen);
    buffer[0] = '\0';

    if(recv(sock,buffer, bufferLen, 0) < 0){
        encerrarPorErro(sock, strerror(errno));
    }

    if(strcmp(buffer, valorEsperado) != 0){
        encerrarPorErro(sock, "o valor esperado não foi recebido");
    }

    free(buffer);    
}

void encerrarPorErro(int sock, char* mensagem){
    #ifdef DEBUG
    fprintf(stderr, "[ERRO] %s\n", mensagem);
    #endif
    encerrarConexao(sock);
    exit(EXIT_FAILURE);
}


void enviarString(int sock, char* valor){
    if(send(sock, valor, strlen(valor), 0) < 0){
        encerrarPorErro(sock, strerror(errno));
    }
}
void enviarInteiro(int sock, int valor){
    int valorRede = htonl(valor);
    if(send(sock,&valorRede, sizeof(valorRede),0) < 0){
        encerrarPorErro(sock, strerror(errno));
    }
}


int lerInteiro(int sock){
    int valorLido = 0;
    if(recv(sock, &valorLido, sizeof(int), 0) < 0){
        encerrarPorErro(sock, strerror(errno));
    }
    return ntohl(valorLido);
}
