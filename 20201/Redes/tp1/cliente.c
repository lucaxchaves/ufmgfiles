#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include "common.h"

#define INET_ADDR "127.0.0.1"


uint8_t recuperaHeaderFrame(int clientFd);

char darPalpite(int clientFd);

void recuperaRespostaPalpite(int clientFd, int* qtdAcertos, char** posicoes);

uint8_t inicializaJogo(int clientFd);

void processaRespostaPalpite(char* palavra, int tamanhoPalavara, char palpite, int qtdAcertos, char* posicoes);
void imprimeEstadoAtual(char* palavra, int tamanhoPalavra);

void finalizaJogo(char* palavra, int tamanhoPalavra, char palpite);
void jogoDaForca(int clientFd);


void utilizacao(){
    printf("Parametros de Inicializacao Invalidos\n");
    printf("Utilizacao: ./cliente <ip-servidor> <porta-servidor>\n");

}


int main(int argc, char** argv){
    if(argc != 3){
        utilizacao();
        return EXIT_FAILURE;
    }
    int clientFd = 0;

	struct sockaddr_storage storage;
	if (0 != addrparse(argv[1], argv[2], &storage)) {
		utilizacao();
        return EXIT_FAILURE;
	}

    if((clientFd = socket(storage.ss_family, SOCK_STREAM, 0)) == -1){
        perror("Falha ao criar o socket.\n");
        return EXIT_FAILURE;
    }


	struct sockaddr *addr = (struct sockaddr *)(&storage);
	if (connect(clientFd, addr, sizeof(storage)) == -1) {
		perror("Falha ao conectar ao server\n");
		return EXIT_FAILURE;
	}

    jogoDaForca(clientFd);
    close(clientFd);
}

void jogoDaForca(int clientFd){
    uint8_t numeroLetras = inicializaJogo(clientFd);
    printf("A palavra que você procura tem %d letra(s)\n",numeroLetras);
    char* palavra = (char*) malloc(sizeof(char) * numeroLetras);
    memset(palavra, 0, numeroLetras);

    bool terminou = false;
    while(!terminou){
        char palpite = darPalpite(clientFd);
        uint8_t headerFrame = 0;
        while (headerFrame == 0)
            headerFrame = recuperaHeaderFrame(clientFd);
        
        if(headerFrame == 3){
            int qtdAcertos = 0;
            char* posicoes = NULL;
            recuperaRespostaPalpite(clientFd, &qtdAcertos, &posicoes);
            processaRespostaPalpite(palavra, numeroLetras, palpite, qtdAcertos, posicoes);
            imprimeEstadoAtual(palavra, numeroLetras);
            if(posicoes!=NULL)
                free(posicoes);
        }else if(headerFrame == 4){
            finalizaJogo(palavra, numeroLetras, palpite);
            terminou = true;
            break;
        }
    }
    if(terminou){
        printf("\n---------FIM-DO-JOGO---------\n\n");
        printf("Parabens voce ganhou!\n");
        printf("Palavra: %s\n", palavra);
        printf("\n-----------------------------\n");
    }
    free(palavra);

}

uint8_t recuperaHeaderFrame(int clientFd){
    uint8_t header= 0;
    recv(clientFd, &header, sizeof(uint8_t), 0);

    return header;
}
char darPalpite(int clientFd){
    char palpite = 0;
    printf("\npalpite> ");
    if(scanf(" %c", &palpite) < 0){
        printf("Erro na leitura.\n");
    } 
    if(palpite >= 97 && palpite <= 122)
        palpite -= 32;

    char framePalpite[2] = {0,0};
    framePalpite[0] = 2;
    framePalpite[1]=palpite;
    send(clientFd, (void*)framePalpite, sizeof(framePalpite) , 0);

    return palpite;
}

void recuperaRespostaPalpite(int clientFd, int* qtdAcertos, char** posicoes){
    (*qtdAcertos) = recuperaHeaderFrame(clientFd);

    if((*qtdAcertos) == 0)
    {
        *posicoes = NULL;
        return;
    }
        
    *posicoes = (char*) malloc(sizeof(char) * (*qtdAcertos));
    memset(*posicoes, 0, (*qtdAcertos));
    recv(clientFd, *posicoes, sizeof(posicoes), 0);

}

uint8_t inicializaJogo(int clientFd){
    char frameInicioJogo[2] = {0,0};
    recv(clientFd, &frameInicioJogo, sizeof(frameInicioJogo), 0);
    
    return (uint8_t) frameInicioJogo[1];
}

void processaRespostaPalpite(char* palavra, int tamanhoPalavra, char palpite, int qtdAcertos, char* posicoes){
    if(qtdAcertos <= 0){
        printf("Nenhum acerto para a letra: %c\n", palpite);    
        return;
    }

    for(int i = 0;i < qtdAcertos; i++){
        uint8_t posicao = posicoes[i];
        if(posicao < tamanhoPalavra){
            palavra[posicao] = palpite;
        }
    }
    printf("Temos %d letra(s) %c\n", qtdAcertos, palpite);
    
}
void imprimeEstadoAtual(char* palavra, int tamanhoPalavra){
    printf("Descoberto até agora: \n");
    for(int i = 0;i < tamanhoPalavra; i++){
        if(palavra[i] == '\0')
            printf("_ ");
        else
            printf("%c ", palavra[i]);         
    }
    printf("\n");
}
void finalizaJogo(char* palavra, int tamanhoPalavra, char palpite){
    for(int i = 0;i < tamanhoPalavra; i++)
        if(palavra[i] == '\0')
            palavra[i] = palpite;
}