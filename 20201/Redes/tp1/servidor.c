#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <sys/socket.h>
#include <pthread.h>

#include <sys/types.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#define PALAVRA "PAMONHA"



void iniciaJogo(int clientFd, uint8_t numeroLetras);

void finalizaJogo(int clientFd);

void enviaRespostaPalpite(int clientFd, uint8_t* posicoes, uint8_t qtdAcertos);

char recebePalpite(int clientFd);

void * jogoDaForca(void* data);

bool jogoFinalizou(char* status, int numeroLetras);


uint8_t* processaPalpite(char palpite, char* palavra, int tamanhoPalavra, char* posicoesAcertadas, uint8_t* qtdAcertos);

struct client_data {
    int csock;
    struct sockaddr_storage storage;
};


int main(int argc, char** argv)
{
    if(argc != 2){
        printf("Parametros de Inicializacao Invalidos\n");
        printf("Utilizacao: ./servidor <porta>\n");
        return EXIT_FAILURE;
    }

    int porta = atoi(argv[1]);
    int serverFd = 0, clientFd = 0;

    struct sockaddr_in6 server;

    if((serverFd = socket(AF_INET6, SOCK_STREAM,0))== -1){
        perror("Falha ao criar o socket");
        return EXIT_FAILURE;
    }

    int busy = 1;
    if(setsockopt(serverFd, SOL_SOCKET, SO_REUSEADDR| SO_REUSEPORT, &busy, sizeof(int))== -1){
        perror("Falha ao setar as  opcoes do Socket");
        return EXIT_FAILURE;
    }

    server.sin6_family = AF_INET6;
    server.sin6_port = htons(porta);
    server.sin6_addr = in6addr_any;
    if(bind(serverFd, (struct sockaddr*) &server, sizeof(server)) == -1){
        perror("Nao foi possivel realizar o bind");
        return EXIT_FAILURE;
    }

    struct timeval timeoutRecv;
    timeoutRecv.tv_sec = 120;
    timeoutRecv.tv_usec = 0;

    struct timeval timeoutSend;
    timeoutSend.tv_sec = 60;
    timeoutSend.tv_usec = 0;

    if(listen(serverFd, 10) == -1){
        perror("Nao foi possivel realizar o listen");
        return EXIT_FAILURE;
    }
       
    while(1){
        struct sockaddr_storage cstorage;
        struct sockaddr *caddr = (struct sockaddr *)(&cstorage);
        socklen_t caddrlen = sizeof(cstorage);

        
        if((clientFd =accept(serverFd, caddr, &caddrlen)) == -1){
            perror("Não foi possivel aceitar a conexao do client");
            
            //Como deu erro ao aceitar a conexao do cliente, passar pro proximo.
            continue;
        }

        if(setsockopt(clientFd, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeoutRecv, sizeof(timeoutRecv)) < 0){
            perror("Não foi possivel definir o timeout de recebimento do client");
            continue;
        }

        if(setsockopt(clientFd, SOL_SOCKET, SO_SNDTIMEO, (char*)&timeoutSend, sizeof(timeoutSend)) < 0){
            perror("Não foi possivel definir o timeout de envio do client");
            continue;
        }

        printf("Novo cliente conectado!\n");
        struct client_data *cdata =(struct client_data*) malloc(sizeof(*cdata));
        cdata->csock = clientFd;
    	memcpy(&(cdata->storage), &cstorage, sizeof(cstorage));
        pthread_t tid;
        pthread_create(&tid, NULL, jogoDaForca, cdata);

    }

    exit(EXIT_SUCCESS);
}

void * jogoDaForca(void * data){
    struct client_data *cdata = (struct client_data *)data;

    int clientFd = cdata->csock; 

    int tamanhoPalavra = strlen(PALAVRA);

    iniciaJogo(clientFd, tamanhoPalavra);
    
    char* posicoesAcertadas = (char*) malloc(sizeof(char)*tamanhoPalavra);
    memset(posicoesAcertadas, 0x0, tamanhoPalavra);

    while(1){
        char palpite = recebePalpite(clientFd);
        uint8_t qtdAcertos = 0;
        uint8_t* posicoes= processaPalpite(palpite, PALAVRA, tamanhoPalavra, posicoesAcertadas, &qtdAcertos);
        if(jogoFinalizou(posicoesAcertadas, tamanhoPalavra)){
            finalizaJogo(clientFd);
            printf("Cliente ganhou!! Desconectando cliente...\n");
            free(posicoes);
            break;
        }
        else{
            enviaRespostaPalpite(clientFd, posicoes, qtdAcertos);
            free(posicoes);
        }       
        
    }
    
    free(posicoesAcertadas);

    
    close(clientFd);
    pthread_exit(EXIT_SUCCESS);
}


bool jogoFinalizou(char* status, int numeroLetras){
    if(status == NULL)
        return false;

    for(int i = 0;i<numeroLetras;i++){
        if(status[i] == 0){
            return false;
        }
    }
    return true;
}


void iniciaJogo(int clientFd, uint8_t numeroLetras){
    char frame_inicio_jogo[2] = {0,0};
    frame_inicio_jogo[0] = 1;
    frame_inicio_jogo[1] = numeroLetras;
    if(send(clientFd, frame_inicio_jogo, sizeof(frame_inicio_jogo) , 0) == -1){
        perror("");
    }
}

void finalizaJogo(int clientFd){
    char frame_fim_jogo = 4;
    if(send(clientFd, (void*) &frame_fim_jogo, sizeof(frame_fim_jogo) , 0) == -1){
        perror("");
    }
}


void enviaRespostaPalpite(int clientFd, uint8_t* posicoes, uint8_t qtdAcertos){
    uint8_t *frame_buffer;
    int tamanhoBuffer = qtdAcertos+2;

    frame_buffer = (uint8_t*) malloc(sizeof(uint8_t)* tamanhoBuffer);
    memset(frame_buffer, 0, tamanhoBuffer);
    frame_buffer[0] = 3;
    frame_buffer[1] = qtdAcertos;

    int lenBuffer = (tamanhoBuffer)*sizeof(uint8_t);

    if(qtdAcertos> 0){
        memcpy(frame_buffer+2,posicoes, qtdAcertos);
    }
    if(send(clientFd, frame_buffer, lenBuffer, 0) == -1){
        perror(""); 
    }
    free(frame_buffer);
}

char recebePalpite(int clientFd){
    char palpite[2] = {0,0};
    
    if(recv(clientFd, &palpite, sizeof(palpite), 0) == -1){
        perror("");
    }

    if(palpite[0] != 2){
        return '\0';
    }


    if(palpite[1] >= 97 && palpite[1] <= 122)
        palpite[1] -= 32;

    return palpite[1];
}

uint8_t* processaPalpite(char palpite, char* palavra, int tamanhoPalavra, char* posicoesAcertadas, uint8_t* qtdAcertos){

    for(int i=0;i<tamanhoPalavra;i++){
        if(palavra[i] == palpite && palpite!='\0'){
            (*qtdAcertos)++;
            posicoesAcertadas[i] = palpite;
        }
    }
    if(*qtdAcertos == 0){
        return NULL;
    }

    uint8_t* acertos = (uint8_t*) malloc((*qtdAcertos)*sizeof(uint8_t));
    memset(acertos, 0, *qtdAcertos);
    int counterAcertos= 0;
    for(int i=0;i<tamanhoPalavra;i++){
        if(counterAcertos >= (*qtdAcertos)) 
                break;
        if(palavra[i] == palpite && palpite!='\0'){
            acertos[counterAcertos++] = i;

        }
    }
    return acertos;
}