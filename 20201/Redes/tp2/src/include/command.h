#pragma once
#include "dns_app.h"

class Command
{
private:
    DnsApp *app;
    void HandleAdd(std::string input);

    void HandleSearch(std::string input);

    void HandleLink(std::string input);

public:
    Command(DnsApp *app);
    ~Command();

    void Execute(std::string input);
};

