#pragma once

#include <map>
#include <string>



class DnsFile {
    private:
        std::map<std::string, std::string>* file;
    public:
        DnsFile();
        ~DnsFile();
        
        void Add(std::string hostname, std::string ip);
        
        std::string* Search(std::string hostname);

};