#pragma once
#include "dns_file.h"
#include <utility>
#include <map>

class DnsApp {

    private:
        DnsFile* dns_file;
        std::map<std::string, int>* linked_servers;

        std::string* ExternalSearch(std::string hostname);

        std::string* InternalSearch(std::string hostname);

        std::string* SearchInServer(std::string hostname, std::string ip, int port);

    public:
        DnsApp();
        ~DnsApp();
        void AddEntry(std::string hostname, std::string ip);
        std::string* Search(std::string hostname);
        void Link(std::string ip, int port);

};
