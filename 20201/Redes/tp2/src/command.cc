#include <string>
#include <cstring>
#include <vector>
#include <iostream>
#include "include/command.h"

std::vector<std::string> split(std::string input, std::string delimiter)
{
    size_t start = 0, end, delim_len = delimiter.length();

    std::vector<std::string> response;
    std::string token;

    while ((end = input.find(delimiter, start)) != std::string::npos)
    {
        token = input.substr(start, end - start);
        start = end + delim_len;
        response.push_back(token);
    }

    response.push_back(input.substr(start));
    return response;
}

Command::Command(DnsApp *app)
{
    this->app = app;
}

Command::~Command()
{
    this->app = nullptr;
}

void Command::HandleAdd(std::string input)
{
    std::vector<std::string> args = split(input, " ");

    if (args.size() != 3)
        return;
    std::string hostname = args[1];
    std::string ip = args[2];
    this->app->AddEntry(hostname, ip);
}

void Command::HandleLink(std::string input)
{
    std::vector<std::string> args = split(input, " ");
    if (args.size() != 3)
        return;
    std::string ip = args[1];
    std::string port_string = args[2];
    try
    {
        int port = std::stoi(port_string);
        this->app->Link(ip, port);
    }  
    catch(...){
        return;
    }
}

void Command::HandleSearch(std::string input)
{
    std::vector<std::string> args = split(input, " ");
    if (args.size() != 2)
        return;
    std::string hostname = args[1];
    std::string *ip = this->app->Search(hostname);
    if (ip != nullptr)
    {
        std::cout << "Found: " << *ip << std::endl;
    }
    else
    {
        std::cout << "Not found" << std::endl;
    }
}

void Command::Execute(std::string input)
{
    if (strncmp(input.c_str(), "add", 3) == 0)
    {
        this->HandleAdd(input);
    }
    else if (strncmp(input.c_str(), "search", 6) == 0)
    {
        this->HandleSearch(input);
    }
    else if (strncmp(input.c_str(), "link", 4) == 0)
    {
        this->HandleLink(input);
    }
}