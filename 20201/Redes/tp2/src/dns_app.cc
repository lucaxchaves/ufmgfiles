#include "include/dns_app.h"
#include "include/dns_file.h"
#include "include/common.h"
#include <iterator>
#include <map>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#define BUFFER_SIZE 1024
DnsApp::DnsApp()
{
    this->dns_file = new DnsFile();
    this->linked_servers = new std::map<std::string, int>();
}
DnsApp::~DnsApp()
{
    delete this->dns_file;
    delete this->linked_servers;
}

void DnsApp::Link(std::string ip, int port)
{
    auto itr = this->linked_servers->find(ip);
    if (itr == this->linked_servers->end())
    {
        this->linked_servers->insert(std::pair<std::string, int>(ip, port));
    }
}

void DnsApp::AddEntry(std::string hostname, std::string ip)
{
    this->dns_file->Add(hostname, ip);
}

std::string *DnsApp::Search(std::string hostname)
{
    std::string *internal_result = InternalSearch(hostname);
    if (internal_result != nullptr)
    {
        return internal_result;
    }
    std::string *external_result = ExternalSearch(hostname);
    if (external_result != nullptr)
    {
        return external_result;
    }
    return nullptr;
}

std::string *DnsApp::InternalSearch(std::string hostname)
{
    return this->dns_file->Search(hostname);
}

std::string *DnsApp::ExternalSearch(std::string hostname)
{

    for (auto itr = this->linked_servers->begin(); itr != this->linked_servers->end(); ++itr)
    {
        std::string *returnValue = SearchInServer(hostname, itr->first, itr->second);
        if (returnValue != nullptr)
        {
            this->AddEntry(hostname, *returnValue);
            return returnValue;
        }
    }
    return nullptr;
}

std::string *DnsApp::SearchInServer(std::string hostname, std::string ip, int port)
{

    struct sockaddr_storage storage;

    if (0 != addrparse(ip.c_str(), port, &storage))
    {
        return nullptr;
    }
    int clientFd = 0;

    if ((clientFd = socket(storage.ss_family, SOCK_DGRAM, 0)) == -1)
    {
        perror("\n[client] socket()");
        return nullptr;
    }

    if (connect(clientFd, (struct sockaddr *)&storage, sizeof(storage)) < 0)
    {
        perror("\n[client] connect()");
        return nullptr;
    }

    int messageLen = hostname.length() + 2;
    
    char *message = (char *)malloc(sizeof(char) * messageLen);
    char *buffer = (char *)malloc(sizeof(char) * BUFFER_SIZE);
    memset(buffer, 0, BUFFER_SIZE);
    memset(message, 0, messageLen);

    message[0] = 1;
    strcpy(message + 1, hostname.c_str());
    ssize_t bytesNet = sendto(clientFd, message, strlen(message), 0, ( struct sockaddr *) NULL, sizeof(storage));
    if (bytesNet <= 0)
    {
        perror("\n[client] sendto()");
        return nullptr;
    }

    bytesNet = recvfrom(clientFd, (char *)buffer, sizeof(buffer), 0, (struct sockaddr *) NULL, NULL);
    if (bytesNet <= 0)
    {
        perror("\n[client] recvfrom()");
        return nullptr;
    }
    if (strlen(buffer) > 1)
    {
        std::string *return_value = new std::string(buffer + 1);
        free(buffer);
        return return_value;
    }
    free(buffer);
    free(message);

    close(clientFd);

    return nullptr;
}