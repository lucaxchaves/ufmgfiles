#include <iostream>
#include <fstream>
#include <string>
#include <thread>
#include <sys/socket.h>
#include <cstdlib>
#include <cstring>
#include <sys/types.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <errno.h>
#include "include/command.h"
#define BUFFER_SIZE 1024
#define HEADER_SIZE 1

void user_commands(DnsApp *server);
void initial_setup(DnsApp *server, std::string input);
void usage()
{
    std::cout << "Parametros invalidosma" << std::endl;
    std::cout << "Utilizacao: ./servidor <porta> [startup]" << std::endl;
}

void listen_dns(DnsApp *dns_server, int port)
{
    int listenFd = 0;
    struct sockaddr_in6 server, cliAddr;

    bzero(&server, sizeof(server));
    if ((listenFd = socket(AF_INET6, SOCK_DGRAM, 0)) == -1)
    {
        perror("\n[server] socket()");
        return;
    }

    int busy = 1;
    if (setsockopt(listenFd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &busy, sizeof(int)) == -1)
    {
        perror("\n[server] setsockopt()");
        return;
    }

    server.sin6_family = AF_INET6;
    server.sin6_port = htons(port);
    server.sin6_addr = (in6addr_any);
    if (bind(listenFd, (struct sockaddr *)&server, sizeof(server)) == -1)
    {
        perror("\n[server] bind()");
        return;
    }

    char *buffer = (char *)malloc(sizeof(char) * BUFFER_SIZE);
    char *request_type = (char *)malloc(sizeof(char));

    socklen_t client_length = sizeof(struct sockaddr_in6);

    while (1)
    {
        ssize_t bytes = recvfrom(listenFd, (char *)buffer, BUFFER_SIZE, 0, (struct sockaddr *)&cliAddr, &client_length);
        if (bytes < 0)
        {
            perror("\n[server](recvfrom)");
            continue;
        }

        std::string hostname = std::string(buffer + 1);
        std::string *ip = dns_server->Search(hostname);
        int size = 2;
        if (ip != nullptr)
        {
            size += ip->length();
        }
        char *response = (char *)malloc(sizeof(char) * (size));
        response[0] = 2;
        if (ip != nullptr)
        {
            strcpy(response + 1, ip->c_str());
        }
        bytes = (sendto(listenFd, response, strlen(response), 0, (const struct sockaddr *)&cliAddr, sizeof(cliAddr)));
        if (bytes < 0)
        {
            perror("\n[server](sendto)");
        }
        free(response);
    }
    free(request_type);
    free(buffer);
    close(listenFd);
}

int main(int argc, char **argv)
{
    if (argc < 2 || argc > 3)
    {
        usage();
        exit(EXIT_FAILURE);
    }
    int port = atoi(argv[1]);
    auto server = new DnsApp();
    if (argc == 3)
    {
        std::string file = std::string(argv[2]);
        initial_setup(server, file);
    }

    std::thread listen_thread(listen_dns, server, port);
    std::thread user_input(user_commands, server);
    listen_thread.join();
    user_input.join();

    return EXIT_SUCCESS;
}

void user_commands(DnsApp *server)
{
    Command command(server);

    for (;;)
    {
        std::string input;
        std::cout << "dns> ";
        std::getline(std::cin, input);

        command.Execute(input);
    }
}

void initial_setup(DnsApp *server, std::string input)
{
    Command command(server);
    std::ifstream input_file;
    input_file.open(input);
    if (!input_file.is_open())
        return;
    std::string line;
    while (std::getline(input_file, line))
    {
        command.Execute(line);
    }
    input_file.close();
}