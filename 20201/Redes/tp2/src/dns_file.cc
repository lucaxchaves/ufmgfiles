#include "include/dns_file.h"

DnsFile::DnsFile()
{
    this->file = new std::map<std::string, std::string>();
}
DnsFile::~DnsFile()
{
    delete this->file;
}

void DnsFile::Add(std::string hostname, std::string ip)
{
    auto itr = this->file->find(hostname);
    if (itr == this->file->end())
    {
        this->file->insert(std::pair<std::string, std::string>(hostname, ip));
    }
}

std::string *DnsFile::Search(std::string hostname)
{
    auto itr = this->file->find(hostname);
    if (itr == this->file->end())
        return nullptr;

    return &itr->second;
}