clear all
close all
clc

%Gerando uma instancia aleatoria do problema da mochila

n = 3000;
p = ceil(10*rand(1,n));
a = ceil(5*rand(1,n));
b = 0.3*sum(a);

model.A = sparse(a);
model.obj = p';
model.rhs = b;
model.sense = '<';
model.vtype = 'B';
model.modelsense = 'max';

result = gurobi(model)

%-------------------------------------------------------------------------
fprintf('Peso: %d/%d | Itens na mochila: \n',sum(a(find(result.x'))),b)
find(result.x')