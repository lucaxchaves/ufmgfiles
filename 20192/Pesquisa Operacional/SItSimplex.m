function SItSimplex(A,b,c,Base)
%=========================================================================
clc
%-------------------------------------------------------------------------
fprintf('Base:');
Base
fprintf('N�o Base:');
NBase = ones(1,size(A,2));
NBase(Base) = 0;
NBase = find(NBase)
%-------------------s------------------------------------------------------
fprintf('Matriz B:');
B = A(:,Base)
fprintf('Matriz N:');
N = A(:,NBase)
fprintf('Matriz B^-1:');
iB = inv(B)
%-------------------------------------------------------------------------
fprintf('Solu��o B�sica:');
xB = iB*b
xN = zeros(length(NBase),1)
f = c([Base NBase])*[xB ; xN]
%-------------------------------------------------------------------------
fprintf('Vetor Multiplicador Simplex:');
pause
lambdaT = c(Base)*iB
fprintf('Custos Reduzidos:');
pause
crNB = c(NBase) - lambdaT*N
%-------------------------------------------------------------------------
[a v] = min(crNB);
entra = NBase(v);
if a >= 0,
    fprintf('PARE!!! Solu��o �tima Encontrada\n\n');
else
    pause
    fprintf('A vari�vel %d entra na base, pois tem custo reduzido mais negativo: %f (Regra de Dantzig).\n\n',entra,a);
    %---------------------------------------------------------------------
    fprintf('Vetor Dire��o Simplex:');
    pause
    gamma = iB*A(:,entra)
    if sum(gamma > 0) == 0,
        fprintf('PARE!!! Solu��o �tima Ilimitada\n\n');
    else
        for i = 1:length(xB),
            if gamma(i) > 0,
                varepsilon(i) = xB(i) / gamma(i);
            else
                varepsilon(i) = inf;
            end
        end
        fprintf('Passo por vari�vel b�sica:');
        pause
        varepsilon = varepsilon'
        %-----------------------------------------------------------------
        [a v] = min(varepsilon);
        sai = Base(v);
        pause
        fprintf('A vari�vel %d sai da base, pois tem menor passo associado: %f.\n\n',sai,a);
        %-----------------------------------------------------------------
        BaseNova = zeros(1,size(A,2));
        BaseNova(Base) = 1;
        BaseNova(sai) = 0;
        BaseNova(entra) = 1;
        fprintf('Nova Base:');
        pause
        BaseNova = find(BaseNova)
        %-----------------------------------------------------------------
%         fprintf('Nova N�o Base:');
%         NBaseNova = ones(1,size(A,2));
%         NBaseNova(BaseNova) = 0;
%         NBaseNova = find(NBaseNova);
        %-----------------------------------------------------------------
        BNova = A(:,BaseNova);
%         NNova = A(:,NBaseNova);
        iBNova = inv(BNova);
        %-----------------------------------------------------------------
        fprintf('Nova Solu��o B�sica:');
        xBNova = iBNova*b
%        xNNova = zeros(length(NBaseNova),1)
        fNova = c([BaseNova])*[xBNova ]
    end
end
%=========================================================================