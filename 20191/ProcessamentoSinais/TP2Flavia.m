close all,clear all,clc;

%% Sele��o do arquivo de entrada.
[filename, pathname] = uigetfile('*.wav', 'Selecione um arquivo de audio para ser filtrado');
filename = fullfile(pathname, filename);

%% Leitura do arquivo.
[audio,Fs] = audioread(filename);

%% Forma de onda do som
N = length(audio); %Numero de amostras
t = (0:N-1) * 1/Fs;


figure('Name','Amplitude x Tempo do �udio Original');
plot(t,audio);
axis([t(1),t(N), -1, 1]);
xlabel('Tempo (s)');
ylabel('Amplitude');

%% Reproduzir o audio original
    sound(audio, Fs);
    
%% Applicacao da FFT
df = Fs/N;
if (mod(Fs,2) == 0) %Se Fs � par
    frequencia_audio = -Fs/2:df:(Fs/2 - df);
else
    frequencia_audio = -Fs/2:df:Fs/2;
end

FFT_audio = fftshift(fft(audio));
H = abs(FFT_audio);
theta = angle(FFT_audio);

%% Informa��es b�sicas do �udio
figure('Name','Frequ�ncia do Audio x Magnitude');
plot(frequencia_audio, H);
axis([frequencia_audio(1),frequencia_audio(N), min(H),max(H)]);

figure('Name','Frequ�ncia do Audio x Fase');
plot(frequencia_audio, theta);
axis([frequencia_audio(1),frequencia_audio(N), min(theta),max(theta)]);



%% Determina��o dos Par�metros a serem utilizados nos filtros.%Ondula��o faixa de passagem
Rp = 0.5; 
%Ondula��o faixa de rejei��o
Rs = 60; 
Fp = Fs/10;
n = ellipord(1,1.1,Rp,Rs,'s'); %Ordem do filtro
[A,B,C,D] = ellip(n,Rp,Rs,1,'s');

%%Cria��o dos filtros
filtros(1)= AplicarFiltro1(A,B,C,D,Fs);
filtros(2)= AplicarFiltro2(A,B,C,D,Fs);
filtros(3) = AplicarFiltro3(A,B,C,D,Fs);
filtros(4) = AplicarFiltro4(A,B,C,D,Fs);
filtros(5) = AplicarFiltro5(A,B,C,D,Fs);

%% Aplica��o do Primeiro Filtro
F1 = tf(filtros(1).numerador,filtros(1).denominador,1/Fs);
%% Polos e Zeros
figure('Name','Diagrama de Polos e Zeros');
pzmap(F1);
axis equal;

%% Respostas de amplitude e fase
figure('Name','Resposta de Amplitude e Fase');
freqz(filtros(1).numerador,filtros(1).denominador);


%% Resposta ao impulso
figure('Name','Resposta ao Impulso');
impulse(F1);

%% Aplica os filtros
K = [1,1,1,1,1]; %Ganho dos filtros
audio_saida = aplicarFiltros(audio, filtros, K);

figure('Name', '�udio ap�s aplica��o dos filtros');
hold on;
plot(t,[audio audio_saida]);
legend('�udio original', '�udio filtrado');
xlabel('Tempo (s)');
ylabel('Amplitude');
hold off;


%% Reproduzir audio de saida
sound(audio_saida, Fs);


%% Dado um audio, um conjunto de filtros e ganhos retorna um audio filtrado.
function audio_saida = aplicarFiltros(audio_original, filtros, ganhos)
    nfiltro = length(filtros);
    nganho = length(ganhos);
    if nganho == nfiltro
        audio_saida = zeros(length(audio_original),1);
        for i = 1:nfiltro
             audio_saida = audio_saida + (ganhos(i)* filter(filtros(i).numerador, filtros(i).denominador, audio_original));
        end
    else
        error('Tamanhos incompat�veis de filtros e ganhos');
    end
end


%% Conjunto de fun��es que retornam filtros
%% Um filtro possui duas informa��es
%% O denominador e numerador da fun��o de transfer�ncia H
function filtro = AplicarFiltro1(A,B,C,D, Fs)
    Wp = 2*Fs*tan(Fs/10*(2*pi/Fs)/2); 
    %Passa baixa para Passa Baixa
    [A1,B1,C1,D1] = lp2lp(A,B,C,D,Wp);
    %ADC Converter
    [A1,B1,C1,D1] = bilinear(A1,B1,C1,D1,Fs); 

    [a1,b1] = ss2tf(A1,B1,C1,D1); 
    filtro.numerador = a1;
    filtro.denominador = b1;
    
end 

function filtro = AplicarFiltro2(A,B,C,D, Fs)
    u1 = 2*Fs*tan(Fs/10*(2*pi/Fs)/2);
    u2 = 2*Fs*tan(2*Fs/10*(2*pi/Fs)/2);
    Bw = u2 - u1;                     
    Wo = sqrt(u1*u2);

    %Passa Baixa para Passa Banda
    [A2,B2,C2,D2] = lp2bp(A,B,C,D,Wo,Bw); 
    [A2,B2,C2,D2] = bilinear(A2,B2,C2,D2,Fs);
    [a2,b2] = ss2tf(A2,B2,C2,D2);


    filtro.numerador = a2;
    filtro.denominador = b2;
    
end 

function filtro = AplicarFiltro3(A,B,C,D, Fs)
    u1 = 2*Fs*tan(2*Fs/10*(2*pi/Fs)/2);
    u2 = 2*Fs*tan(3*Fs/10*(2*pi/Fs)/2);
    Bw = u2 - u1;                     
    Wo = sqrt(u1*u2);
    
    %Passa Baixa para Passa Banda
    [A3,B3,C3,D3] = lp2bp(A,B,C,D,Wo,Bw);
    [A3,B3,C3,D3] = bilinear(A3,B3,C3,D3,Fs);
    [a3,b3] = ss2tf(A3,B3,C3,D3);

    filtro.numerador = a3;
    filtro.denominador = b3;
    
end 

function filtro = AplicarFiltro4(A,B,C,D, Fs)
    u1 = 2*Fs*tan(3*Fs/10*(2*pi/Fs)/2);
    u2 = 2*Fs*tan(4*Fs/10*(2*pi/Fs)/2);
    Bw = u2 - u1;                     
    Wo = sqrt(u1*u2);
    [A4,B4,C4,D4] = lp2bp(A,B,C,D,Wo,Bw);
    [A4,B4,C4,D4] = bilinear(A4,B4,C4,D4,Fs);
    [a4,b4] = ss2tf(A4,B4,C4,D4);

    filtro.numerador = a4;
    filtro.denominador = b4;
end 


function filtro = AplicarFiltro5(A,B,C,D, Fs)
   
    Wp = 2*Fs*tan(4*Fs/10*(2*pi/Fs)/2);
    %Passa Baixa para Passa Alta
    [A5,B5,C5,D5] = lp2hp(A,B,C,D,Wp);
    % ADC Converter
    [A5,B5,C5,D5] = bilinear(A5,B5,C5,D5,Fs);
    [a5,b5] = ss2tf(A5,B5,C5,D5);

    filtro.numerador = a5;
    filtro.denominador = b5;
    
end 

