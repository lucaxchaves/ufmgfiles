%% Fecha as janelas, limpa as varaiveis e o terminal de comando
close all;
clear all;
clc;

%% Forma de onda do som
[audio,Fs] = audioread('Blind_intro.wav');
N = length(audio); %Numero de amostras
t = (0:N-1) * 1/Fs;
figure;
plot(t,audio);
axis([t(1),t(N), -1, 1]);
xlabel('Tempo (s)');
ylabel('Amplitude');

%% Reproduzir o som
if false %Coloque true para reproduzir
    sound(audio, Fs);
end

%% FFT
df = Fs/N;
if (mod(Fs,2) == 0) %Se Fs � par
    frequencia_audio = -Fs/2:df:(Fs/2 - df);
else
    frequencia_audio = -Fs/2:df:Fs/2;
end
FFT_audio = fftshift(fft(audio));
H = abs(FFT_audio);
teta = angle(FFT_audio);

figure;
plot(frequencia_audio, H);
axis([frequencia_audio(1),frequencia_audio(N), min(H),max(H)]);
figure;
plot(frequencia_audio, teta);
axis([frequencia_audio(1),frequencia_audio(N), min(teta),max(teta)]);


%% Filtro IIR
Rp = 0.5; %Ondula��o desejada na faixa de passagem
Rs = 60; %Ondula��o desejada na faixa de rejei��o
Fp = Fs/10; %Frequ�ncia desejada


%Filtro analogico com frequencia de corte de 1
n = ellipord(1,1.1,Rp,Rs,'s'); %Ordem do filtro
[A,B,C,D] = ellip(n,Rp,Rs,1,'s');
[a,b] = ss2tf(A,B,C,D);

%Filtro 1
Wp = 2*Fs*tan(Fs/10*(2*pi/Fs)/2); %Corrigir o erro devido ao "arqueamento"  
[A1,B1,C1,D1] = lp2lp(A,B,C,D,Wp); %Converte o filtro para passa baixa com corte de Fs/10
[A1,B1,C1,D1] = bilinear(A1,B1,C1,D1,Fs); %Converte o filtro para digital


%Fun��o de transferencia
[a1,b1] = ss2tf(A1,B1,C1,D1); 
F1 = tf(a1,b1,1/Fs);

%Polos e zeros
figure;
pzmap(F1);
axis equal;

%Respostas de amplitude e fase
figure;
freqz(a1,b1);

%Resposta ao impulso
figure;
impulse(F1);


%% Banco de Filtro
%Filtro 2
u1 = 2*Fs*tan(Fs/10*(2*pi/Fs)/2);
u2 = 2*Fs*tan(2*Fs/10*(2*pi/Fs)/2);
Bw = u2 - u1;                     
Wo = sqrt(u1*u2);
[A2,B2,C2,D2] = lp2bp(A,B,C,D,Wo,Bw); %Converte um passa baixa, para banda passante 
[A2,B2,C2,D2] = bilinear(A2,B2,C2,D2,Fs);
[a2,b2] = ss2tf(A2,B2,C2,D2);
%figure;
%freqz(a2,b2);

%Filtro 3
u1 = 2*Fs*tan(2*Fs/10*(2*pi/Fs)/2);
u2 = 2*Fs*tan(3*Fs/10*(2*pi/Fs)/2);
Bw = u2 - u1;                     
Wo = sqrt(u1*u2);
[A3,B3,C3,D3] = lp2bp(A,B,C,D,Wo,Bw);
[A3,B3,C3,D3] = bilinear(A3,B3,C3,D3,Fs);
[a3,b3] = ss2tf(A3,B3,C3,D3);
%figure;
%freqz(a3,b3);

%Filtro 3
u1 = 2*Fs*tan(3*Fs/10*(2*pi/Fs)/2);
u2 = 2*Fs*tan(4*Fs/10*(2*pi/Fs)/2);
Bw = u2 - u1;                     
Wo = sqrt(u1*u2);
[A4,B4,C4,D4] = lp2bp(A,B,C,D,Wo,Bw);
[A4,B4,C4,D4] = bilinear(A4,B4,C4,D4,Fs);
[a4,b4] = ss2tf(A4,B4,C4,D4);
%figure;
%freqz(a4,b4);

%Filtro 5
Wp = 2*Fs*tan(4*Fs/10*(2*pi/Fs)/2);
[A5,B5,C5,D5] = lp2hp(A,B,C,D,Wp); %Converte um passa baixa, para passa altas 
[A5,B5,C5,D5] = bilinear(A5,B5,C5,D5,Fs);
[a5,b5] = ss2tf(A5,B5,C5,D5);
%figure;
%freqz(a5,b5);



%% Aplica o filtro
K = [1,1,1,1,1]; %Ganho dos filtros
y1 = K(1) * filter(a1,b1,audio);
y2 = K(2) * filter(a2,b2,audio);
y3 = K(3) * filter(a3,b3,audio);
y4 = K(4) * filter(a4,b4,audio);
y5 = K(5) * filter(a5,b5,audio);
audio_saida = y1 + y2 + y3 + y4 + y5;
%figure;
plot(t,[audio audio_saida]);


%% Reproduzir audio de saida
if true %Coloque true para reproduzir
    sound(audio_saida, Fs);
end