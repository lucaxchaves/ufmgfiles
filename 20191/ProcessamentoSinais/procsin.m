% Este script permite abrir e visualizar os dados.

close all;
clear all;

%[arquivo, caminho] = uigetfile('*.*');
%if ~arquivo
%    return;
%end
nome = 'C:\Users\lucas\Projetos\ufmgfiles\20191\ProcessamentoSinais\TEK00000.CSV'
%-- sprintf'%s%s', caminho, arquivo);

dados = load (nome);
vetTempos = dados(:,1) - dados(1,1);
sinal = dados(:,2);
%sinal = bandpass(sinal, [1e3 1e6],2e6);
media = mean(sinal);

fs = 1/(vetTempos(2) - vetTempos(1));
N = length(sinal);


Y = 1/N .* fft(sinal - media)';
absY = abs([Y(N/2+1:N) Y(1:N/2)]);
vetFrequencias = [-N/2:N/2-1] .* fs/N;  

figure;
subplot(211);
plot(vetTempos, sinal  , 'k');
hold on;
plot(vetTempos, media .* ones(1, N), 'r--');
axis([vetTempos(1) vetTempos(length(vetTempos)) 1.2 * min(sinal) 1.2 * max(sinal)]);
xlabel('Time (s)');
ylabel('Amplitude (V)');
texto = sprintf('M�dia = %.3f', media);
temp = axis();
text(temp(1)+3/4*(temp(2)-temp(1)), temp(3)+3/4*(temp(4)-temp(3)), texto);


subplot(212);
plot(vetFrequencias, absY, 'k');
hold on;
plot(vetFrequencias, absY(N/2+1) .* ones(1,N), 'r--');
axis([vetFrequencias(1) vetFrequencias(length(vetFrequencias)) 0 1.2 * max(absY)]);
xlabel('Frequency');
ylabel('Amplitude');
texto = sprintf('Comp. DC = %.3f', absY(N/2+1));
temp = axis();
text(temp(1)+3/4*(temp(2)-temp(1)), temp(3)+3/4*(temp(4)-temp(3)), texto);

%filtro = designfilt('bandstopiir','FilterOrder',2,'PassbandFrequency1',1000,'PassbandFrequency2',1000000, ...
%               'DesignMethod','chebyl','SampleRate',2000000);
filtro = designfilt('bandstopiir', 'PassbandFrequency1', 1, 'StopbandFrequency1', 59, ...
           'StopbandFrequency2', 61, 'PassbandFrequency2', 1000000, 'PassbandRipple1', 5.0012, ...
           'StopbandAttenuation', 200, 'PassbandRipple2', 1, 'SampleRate', 2000000);

%filtro = designfilt('bandpassiir', 'StopbandFrequency1', 10, 'PassbandFrequency1', 1000, 'PassbandFrequency2', 1000000, 'StopbandFrequency2', 10000000, 'StopbandAttenuation1', 200, 'PassbandRipple', 1, 'StopbandAttenuation2', 200, 'SampleRate', 70000000);

sinal_60Hz = bandpass(filter(filtro,sinal),[1000 1000000],2000000);
figure;

plot(vetTempos, sinal_60Hz  , 'k');
hold on;
plot(vetTempos, media .* ones(1, N), 'r--');
axis([vetTempos(1) vetTempos(length(vetTempos)) 1.2 * min(sinal_60Hz) 1.2 * max(sinal_60Hz)]);
xlabel('Time (s)');
ylabel('Amplitude (V)');
texto = sprintf('M�dia = %.3f', media);
temp = axis();
text(temp(1)+3/4*(temp(2)-temp(1)), temp(3)+3/4*(temp(4)-temp(3)), texto);






