#################################################################
#Bubble Sort em MIPS						#
#Arquitetura e Organiza��o de Computadores - UFMG		#
#Lucas Silva Chaves - 2017001737 - 03/04			#
#################################################################
.data
.align 2
vetor: .word 9,1,18,6,2,4,3,2,11,0
.text
main:
	addi $sp, $sp, -4, #Adiciona uma posi��o no stack pointer
	sw $ra,4($sp)	   #Salva o $ra
	la $a0, vetor	   #Carrega o endere�o do vetor em $a0
	li $a1, 0xa	   #Carrega o tamanho do vetor em $a1
	jal bubble_sort    #Chama a sub-rotina bubble_sort
	addi $sp, $sp, 4   #Libera uma posicao no stack
	j done		   #Finaliza o programa

#void bubble_sort(vetor, n)
#Primeiro endere�o do vetor esteja em $a0
#Ultimo index do vetor esteja em $a1
bubble_sort:
#j = $t0
#k = $t1
#aux = $t2

	addi $sp, $sp, -4 #Adicionamos uma posicao no stack pointer
	sw $ra, 4($sp)    #Salva o $ra
	li $t0, 0x1	  #Inicializa j = 1
	subi $t3, $a1, 1  # n - 1
forJ:
	bge $t0,$a1, fimForJ # se j > n vai para fimForJ
	li $t1,0x0	     # Inicializa k = 0 
forK:
	bge $t1, $t3, fimForK	# se j > n -1 vai para fimForK
	sll $t9, $t1, 2		# Calculo da posicao j do vetor
	add $t6, $a0, $t9	# C�lculo do endere�o vetor[j] 
	addi $t7 $t6, 4		# C�lculo do endere�o vetor[j+1]
	lw $t4, 0($t6)		# Armazenamento de vetor[j] em registrador $t4
	lw $t5, 0($t7)		# Armazenamento de vetor[k] em registrador $t5
	ble $t4, $t5, endIf	# Se $t4 <= $t5 vai para o endIf
	move $t2, $t4		# aux($t2) = $t4
	sw $t5, ($t6)		# vetor[j] =  $t5
	sw $t2, ($t7)		# vetor[j+1] = aux
endIf:
	addi $t1, $t1, 1	# k++
	j forK			
fimForK:
	addi $t0, $t0, 1 	# j++
	j forJ
fimForJ:
	lw $ra, 4($sp)		# Recupera $ra
	addi $sp, $sp,4		# Libera uma posi��o no stack
	jr $ra	
done:

