rm(list=ls())
source('trainMLP.R')
x_train <- seq(from=0, to=2*pi, by=0.15)
x_train <- x_train + (runif(length(x_train))-0.5)/5
  
i <- sample(length(x_train))
y_train <- sin(x_train)
y_train <- y_train + (runif(length(y_train))-0.5)/5
  

x_test <- seq(from=0, to=2*pi, by=0.01)
x_test <- x_test + (runif(length(x_test))-0.5)/5

y_test <- sin(x_test)

plot(x_train, y_train, col='blue', xlim=c(0,2*pi), ylim=c(-1,1), xlab='x', ylab='y')
par(new=T)
plot(x_test, y_test, type='l',col='red', xlim=c(0,2*pi), ylim=c(-1,1), xlab='', ylab='', xaxt='n', yaxt='n')


estrutura_rede = c(1, 3, 1)
funcoes_ativacao <- c(tanh,identity)
# Estrutura da rede é definida por
# (N0,N1,...,Nk,Nm)
# N0: Numero de Entradas
# 
# N1: Numero de Neuronios na 1 Camada escondida
# Nk: Numero de Neuronios na K Camada escondida
# 
# Nm: Numero de Saídas


pesos <- gerar_pesos(estrutura_rede)
funcoes_rede <- c(tanh, identity)
avaliacao_rede(X, pesos, funcoes_rede)




