rm(list=ls())
library('plot3D')
source('trainperceptron.R')
source('yperceptron.R')

s1<-0.4
s2<-0.4

nc<-200

x1<-matrix(rnorm(nc*2), ncol=2)*s1 + t(matrix((c(2,2)),ncol=nc, nrow=2))
x2<-matrix(rnorm(nc*2), ncol=2)*s2 + t(matrix((c(4,4)),ncol=nc, nrow=2))


x<- rbind(x1,x2)
x<- cbind(x,1)

x[1:nc,3]<-0 
  
plot(x[1:nc,1], x[1:nc,2], col='red', xlim=c(0,6), ylim=c(0,6), xlab = 'x_1', ylab = 'x_2')
par(new=T)
plot(x[(nc+1):(nc*2),1], x[(nc+1):(nc*2),2], col='blue', xlim=c(0,6), ylim=c(0,6), xlab = '', ylab = '')



x1_reta <- seq(6/100, 6, 6/100)
X2_reta <- -x1_reta +6

par(new=T)
plot(x1_reta, X2_reta, type='l', col='orange', xlim=c(0,6), ylim=c(0,6), xlab='', ylab='')


seqTreinamento <- sample(1:nc, nc*0.9)
seqTreinamento <- c(seqTreinamento, sample((nc+1):(nc*2), nc*0.9))

treinamento <- treinap(as.matrix(x[seqTreinamento,1:2]), as.matrix(x[seqTreinamento,3]),0.01,0.0001,100,1)

w<- treinamento[[1]]


seqi <- seq(0,6,0.1)
seqj <- seq(0,6,0.1)
z <- matrix(0,nrow=length(seqi), ncol=length(seqj))
ci<-0
for(i in seqi){
  ci<-ci+1
  cj<-0
  for(j in seqj){
    cj<-cj+1
    xv<-c(1,i,j)
    z[ci,cj]<-yp(xv,w,0)
  }
}

plot(x[1:nc,1], x[1:nc,2], col='red', xlim=c(0,6), ylim=c(0,6), xlab = 'x_1', ylab = 'x_2')
par(new=T)
plot(x[(nc+1):(nc*2),1], x[(nc+1):(nc*2),2], col='blue', xlim=c(0,6), ylim=c(0,6), xlab = '', ylab = '')
par(new=T)
contour(seqi, seqj, z, xlim=c(0,6), ylim=c(0,6),xlab='',ylab='',drawlabels = FALSE)

persp3D(seqi, seqj, z, counter=T,theta=15, phi=20, r=50,d=0.1, expand=0.5,ltheta=90, lphi=180, shade=0.4,ticktype='detailed', nticks=1)
