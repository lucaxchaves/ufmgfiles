rm(list=ls())
library('mlbench')
library('modelr')

set.seed(1)

source('trainperceptron.R')
source('yperceptron.R')

data('BreastCancer')
data2<- BreastCancer
#Data Clean
data2<- data2[complete.cases(data2),]
levels(data2[["Class"]]) <- c(1,0) #Set different levels, instead of malign and benign


folds <- crossv_kfold(data2, k=10)
accuracy<-c()
i<-1
for(i in c(1:10)){
 #Cleaning Kth Fold
  trainDataSeq <- folds$train[[i]]$idx
  trainData<-apply(as.matrix.noquote(data2[trainDataSeq,2:10]),2,as.numeric)
  trainLabel<-apply(as.matrix.noquote(data2[trainDataSeq,11]),2,as.numeric)
  
  testDataSeq <- folds$test[[i]]$idx
  testData<-apply(as.matrix.noquote(data2[testDataSeq,2:10]),2,as.numeric)
  testLabel<-apply(as.matrix.noquote(data2[testDataSeq,11]),2,as.numeric)
  
  #Training Perceptron
  treinamento <- treinap(trainData, trainLabel,0.01,0.0001,100,1)
  pesos <- treinamento[[1]]

  #Prediction with returned Weights from Perceptron
  predictedLabels <- yp(testData,pesos,1)
  accuracy<- c(accuracy,length(which(predictedLabels == testLabel)) / length(testLabel))
 
}
