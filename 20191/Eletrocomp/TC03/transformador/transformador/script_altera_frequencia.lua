for f = 60, 600, 60 do
	mi_probdef(f,"inches", "planar", 1e-008, 1.625 ,20 , ("Succ."))
	mi_analyze(0)
	mi_loadsolution()
	mo_groupselectblock(1)
    print(f, mo_blockintegral(6))
end