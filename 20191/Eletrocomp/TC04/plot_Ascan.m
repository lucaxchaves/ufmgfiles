% plot_Ascan.m
% Script to save and plot EM fields from a gprMax A-scan
%
% Craig Warren

clear all; 
clc;

%[filename, pathname] = uigetfile('*.out', 'Select gprMax A-scan output file to plot');

fullfilename = char("C:\Users\lucas\Projetos\ufmgfiles\20191\Eletrocomp\TC04\Q3\entrada3.out");

%disp(filename);

if fullfilename ~= 0
    header.title = h5readatt(fullfilename, '/', 'Title');
    header.iterations = double(h5readatt(fullfilename,'/', 'Iterations'));
    %tmp = h5readatt(fullfilename, '/', 'dx_dy_dz');
    header.dx = 0.002;
    header.dy = 0.002;
    header.dz = 0.002;
    header.dt = h5readatt(fullfilename, '/', 'dt');
    header.nsrc = h5readatt(fullfilename, '/', 'nsrc');
    header.nrx = h5readatt(fullfilename, '/', 'nrx');

    % Time vector for plotting
    time = linspace(0, (header.iterations - 1) * header.dt, header.iterations)';

    % Initialise structure for field arrays
    fields.ex = zeros(header.iterations, header.nrx);
    fields.ey = zeros(header.iterations, header.nrx);
    fields.ez = zeros(header.iterations, header.nrx);
    fields.hx = zeros(header.iterations, header.nrx);
    fields.hy = zeros(header.iterations, header.nrx);
    fields.hz = zeros(header.iterations, header.nrx);
    

    % Save and plot fields from each receiver
    for n=1:header.nrx
        path = strcat('/rxs/rx', num2str(n));
        tmp = h5readatt(fullfilename, path, 'Position');
        header.rx(n) = tmp(1);
        header.ry(n) = tmp(2);
        header.rz(n) = tmp(3);
        path = strcat(path, '/');
        fields.ex(:,n) = h5read(fullfilename, strcat(path, 'Ex'));
        fields.ey(:,n) = h5read(fullfilename, strcat(path, 'Ey'));
        fields.ez(:,n) = h5read(fullfilename, strcat(path, 'Ez'));
        fields.hx(:,n) = h5read(fullfilename, strcat(path, 'Hx'));
        fields.hy(:,n) = h5read(fullfilename, strcat(path, 'Hy'));
        fields.hz(:,n) = h5read(fullfilename, strcat(path, 'Hz'));
    end
end


  
for i = 1:header.nrx
    hold on
    xlabel('Tempo (s)')
    ylabel('Ez (V/m)')
    E= plot(time, fields.ez(:,i));
    set(E, 'linewidth',2);
    grid on
    name = ['E_z do receptor ' int2str(i)];
    set(E, 'linewidth',3);
    set(gca,'fontsize',20);
    legend('RX_1', 'RX_2')
    hold off
end