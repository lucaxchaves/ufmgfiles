% plot_Ascan.m
% Script to save and plot EM fields from a gprMax A-scan
%
% Craig Warren

clear all; 
clc;

%[filename, pathname] = uigetfile('*.out', 'Select gprMax A-scan output file to plot');

fullfilename = char("C:\Users\lucas\Projetos\ufmgfiles\20191\Eletrocomp\TC04\Q4\TC04_analysismodel40.out");
fullfilename2 = char("C:\Users\lucas\Projetos\ufmgfiles\20191\Eletrocomp\TC04\Q4\TC04_calibration.out");

%disp(filename);

if fullfilename ~= 0
    header.title = h5readatt(fullfilename, '/', 'Title');
    header.iterations = double(h5readatt(fullfilename,'/', 'Iterations'));
    %tmp = h5readatt(fullfilename, '/', 'dx_dy_dz');
    header.dx = 0.002;
    header.dy = 0.002;
    header.dz = 0.002;
    header.dt = h5readatt(fullfilename, '/', 'dt');
    header.nsrc = h5readatt(fullfilename, '/', 'nsrc');
    header.nrx = h5readatt(fullfilename, '/', 'nrx');

    % Time vector for plotting
    time = linspace(0, (header.iterations - 1) * header.dt, header.iterations)';

    % Initialise structure for field arrays
    fields.ex = zeros(header.iterations, header.nrx);
    fields.ey = zeros(header.iterations, header.nrx);
    fields.ez = zeros(header.iterations, header.nrx);
    fields.hx = zeros(header.iterations, header.nrx);
    fields.hy = zeros(header.iterations, header.nrx);
    fields.hz = zeros(header.iterations, header.nrx);
    

    % Save and plot fields from each receiver
    for n=1:header.nrx
        path = strcat('/rxs/rx', num2str(n));
        tmp = h5readatt(fullfilename, path, 'Position');
        header.rx(n) = tmp(1);
        header.ry(n) = tmp(2);
        header.rz(n) = tmp(3);
        path = strcat(path, '/');
        fields.ex(:,n) = h5read(fullfilename, strcat(path, 'Ex'));
        fields.ey(:,n) = h5read(fullfilename, strcat(path, 'Ey'));
        fields.ez(:,n) = h5read(fullfilename, strcat(path, 'Ez'));
        fields.hx(:,n) = h5read(fullfilename, strcat(path, 'Hx'));
        fields.hy(:,n) = h5read(fullfilename, strcat(path, 'Hy'));
        fields.hz(:,n) = h5read(fullfilename, strcat(path, 'Hz'));
    end
end

if fullfilename2 ~= 0
    header2.title = h5readatt(fullfilename2, '/', 'Title');
    header2.iterations = double(h5readatt(fullfilename2,'/', 'Iterations'));
    %tmp = h5readatt(fullfilename, '/', 'dx_dy_dz');
    header2.dx = 0.002;
    header2.dy = 0.002;
    header2.dz = 0.002;
    header2.dt = h5readatt(fullfilename2, '/', 'dt');
    header2.nsrc = h5readatt(fullfilename2, '/', 'nsrc');
    header2.nrx = h5readatt(fullfilename2, '/', 'nrx');

    % Time vector for plotting
    time2 = linspace(0, (header2.iterations - 1) * header2.dt, header2.iterations)';

    % Initialise structure for field arrays
    fields2.ex = zeros(header2.iterations, header2.nrx);
    fields2.ey = zeros(header2.iterations, header2.nrx);
    fields2.ez = zeros(header2.iterations, header2.nrx);
    fields2.hx = zeros(header2.iterations, header2.nrx);
    fields2.hy = zeros(header2.iterations, header2.nrx);
    fields2.hz = zeros(header2.iterations, header2.nrx);
    

    % Save and plot fields from each receiver
    for n=1:header2.nrx
        path = strcat('/rxs/rx', num2str(n));
        tmp = h5readatt(fullfilename2, path, 'Position');
        header2.rx(n) = tmp(1);
        header2.ry(n) = tmp(2);
        header2.rz(n) = tmp(3);
        path = strcat(path, '/');
        fields2.ex(:,n) = h5read(fullfilename2, strcat(path, 'Ex'));
        fields2.ey(:,n) = h5read(fullfilename2, strcat(path, 'Ey'));
        fields2.ez(:,n) = h5read(fullfilename2, strcat(path, 'Ez'));
        fields2.hx(:,n) = h5read(fullfilename2, strcat(path, 'Hx'));
        fields2.hy(:,n) = h5read(fullfilename2, strcat(path, 'Hy'));
        fields2.hz(:,n) = h5read(fullfilename2, strcat(path, 'Hz'));
    end
end


  
for i = 1:header.nrx
    hold on
    xlabel('Tempo (s)')
    ylabel('Ez (V/m)')
    E(1)= plot(time, fields.ez(:,i));
    E(2)= plot(time2, fields2.ez(:,i));
    set(E, 'linewidth',2);
    grid on
    name = ['E_z do receptor ' int2str(i)];
    set(E, 'linewidth',3);
    set(gca,'fontsize',20);
    legend('analysismodel40.out', 'calibration.out')
    hold off
end