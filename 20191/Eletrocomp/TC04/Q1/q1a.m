%Poynting - q1a
%Lucas Chaves - Lucas Bispo

clear all, clc

%[filename, pathname] = uigetfile('*.out', 'Select gprMax A-scan output file to plot');

fullfilename = char("C:\Users\lucas\Projetos\ufmgfiles\20191\Eletrocomp\TC04\matlab\q1a.out");
disp(fullfilename);
%disp(filename);

if fullfilename ~= 0
    header.title = h5readatt(fullfilename, '/', 'Title');
    header.iterations = double(h5readatt(fullfilename,'/', 'Iterations'));
    tmp = h5readatt(fullfilename, '/', 'dx_dy_dz');
    header.dx = tmp(1);
    header.dy = tmp(2);
    header.dz = tmp(3);
    header.dt = h5readatt(fullfilename, '/', 'dt');
    header.nsrc = h5readatt(fullfilename, '/', 'nsrc');
    header.nrx = h5readatt(fullfilename, '/', 'nrx');

    % Time vector for plotting
    time = linspace(0, (header.iterations - 1) * header.dt, header.iterations)';

    % Initialise structure for field arrays
    fields.ex = zeros(header.iterations, header.nrx);
    fields.ey = zeros(header.iterations, header.nrx);
    fields.ez = zeros(header.iterations, header.nrx);
    fields.hx = zeros(header.iterations, header.nrx);
    fields.hy = zeros(header.iterations, header.nrx);
    fields.hz = zeros(header.iterations, header.nrx);
    

    % Save and plot fields from each receiver
    for n=1:header.nrx
        path = strcat('/rxs/rx', num2str(n));
        tmp = h5readatt(fullfilename, path, 'Position');
        header.rx(n) = tmp(1);
        header.ry(n) = tmp(2);
        header.rz(n) = tmp(3);
        path = strcat(path, '/');
        fields.ex(:,n) = h5read(fullfilename, strcat(path, 'Ex'));
        fields.ey(:,n) = h5read(fullfilename, strcat(path, 'Ey'));
        fields.ez(:,n) = h5read(fullfilename, strcat(path, 'Ez'));
        fields.hx(:,n) = h5read(fullfilename, strcat(path, 'Hx'));
        fields.hy(:,n) = h5read(fullfilename, strcat(path, 'Hy'));
        fields.hz(:,n) = h5read(fullfilename, strcat(path, 'Hz'));

    end
    
    
    % Poynting
    E = cell(4,1);
    for i = 1:4
        E{i} = [zeros(637,1), zeros(637,1), fields.ez(:,i)];
    end

    H = cell(4,1);
    for i = 1:4
        H{i} = [fields.hx(:,i), fields.hy(:,i), zeros(637,1)];
    end 

    P = cell(4,1);
    for i = 1:4
        P{i} = cross(E{i},H{i});
    end

    % Plot Poynting
    for i = 1:4
        figure('Name', ['Poynting_do_receptor_' num2str(i)])
        hold on
        xlabel('Tempo (s)')
        ylabel('(W/m�)')
        H = plot(time, P{i});
        set(H, 'linewidth',2);
        grid on
        name = ['Poynting_do_receptor_' int2str(i)];
        set(H, 'linewidth',3);
        set(gca,'fontsize',20);
        hold off
    end
end
