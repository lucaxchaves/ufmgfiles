%Poynting - q1a
%Lucas Chaves - Lucas Bispo

clear all, clc, close all;

%[filename, pathname] = uigetfile('*.out', 'Select gprMax A-scan output file to plot');

fullfilename = char("C:\Users\lucas\Projetos\ufmgfiles\20191\Eletrocomp\TC04\Q2\Q2.out");
disp(fullfilename);
%disp(filename);

if fullfilename ~= 0
    header.title = h5readatt(fullfilename, '/', 'Title');
    header.iterations = double(h5readatt(fullfilename,'/', 'Iterations'));
    tmp = h5readatt(fullfilename, '/', 'dx_dy_dz');
    header.dx = tmp(1);
    header.dy = tmp(2);
    header.dz = tmp(3);
    header.dt = h5readatt(fullfilename, '/', 'dt');
    header.nsrc = h5readatt(fullfilename, '/', 'nsrc');
    header.nrx = h5readatt(fullfilename, '/', 'nrx');

    % Time vector for plotting
    time = linspace(0, (header.iterations - 1) * header.dt, header.iterations)';

    % Initialise structure for field arrays
    fields.ex = zeros(header.iterations, header.nrx);
    fields.ey = zeros(header.iterations, header.nrx);
    fields.ez = zeros(header.iterations, header.nrx);
    fields.hx = zeros(header.iterations, header.nrx);
    fields.hy = zeros(header.iterations, header.nrx);
    fields.hz = zeros(header.iterations, header.nrx);
    

    % Save and plot fields from each receiver
    for n=1:header.nrx
        path = strcat('/rxs/rx', num2str(n));
        tmp = h5readatt(fullfilename, path, 'Position');
        header.rx(n) = tmp(1);
        header.ry(n) = tmp(2);
        header.rz(n) = tmp(3);
        path = strcat(path, '/');
        fields.ex(:,n) = h5read(fullfilename, strcat(path, 'Ex'));
        fields.ey(:,n) = h5read(fullfilename, strcat(path, 'Ey'));
        fields.ez(:,n) = h5read(fullfilename, strcat(path, 'Ez'));
        fields.hx(:,n) = h5read(fullfilename, strcat(path, 'Hx'));
        fields.hy(:,n) = h5read(fullfilename, strcat(path, 'Hy'));
        fields.hz(:,n) = h5read(fullfilename, strcat(path, 'Hz'));

    end
    
%     for i = 1:3
%         %figure ('Name', ['E_z_do_receptor_' num2str(i)]) 
%         hold on
%         xlabel('Tempo (s)')
%         ylabel('Ez (V/m)')
%         E = plot(fields.t, fields.ez(:,i));
%         set(E, 'linewidth',2);
%         grid on
%         name = ['E_z do receptor ' int2str(i)];
%         set(E, 'linewidth',3);
%         set(gca,'fontsize',20);
%         legend('RX1', 'RX2', 'RX3')
%         hold off
%     end
% 
% 
%     % Plot Hx
%     for i = 1:3
%         figure('Name', ['Hx_receptor_' num2str(i)])
%         hold on
%         xlabel('Tempo (s)')
%         ylabel('Hx (A/m)')
%         H = plot(fields.t, fields.hx(:,i));
%         set(H, 'linewidth',2);
%         grid on
%         name = ['Hx_do_receptor_' int2str(i)];
%         set(H, 'linewidth',3);
%         set(gca,'fontsize',20);
%         hold off
%     end

    % Plot Hy
    figure('Name',['E'])
    for i = 1:3
        %figure('Name', ['Hy_receptor_' num2str(i)])
        hold on
        xlabel('Tempo (s)')
        ylabel('E_z (V/m)')
        H = plot(time, fields.ez(:,i));
        set(H, 'linewidth',2);
        grid on
        name = ['E_z do_receptor_' int2str(i)];
        set(H, 'linewidth',3);
        legend('RX_1','RX_2','RX_3')
        set(gca,'fontsize',20);
        hold off
    end
        figure('Name', 'H')
    for i = 1:3
        %figure('Name', ['Hy_receptor_' num2str(i)])
        hold on
        xlabel('Tempo (s)')
        ylabel('H_y (A/m)')
        H = plot(time, fields.hy(:, i));
        set(H, 'linewidth',2);
        grid on
        name = ['Hy_do_receptor_' int2str(i)];
        set(H, 'linewidth',3);
        set(gca,'fontsize',20);
        legend('RX_1','RX_2','RX_3')
        hold off
    end

end
