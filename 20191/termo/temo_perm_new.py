import numpy as np
import matplotlib.pyplot as plt
import copy

def calc_perm(p):
    #const
    k,Qp,h,ti = 15, 2000, 40, 25+273
    b_e = [24,36,48,57,66]
    deltaX, deltaY = 0.5,0.5
    pn = p-1
    eq = np.zeros((75,))
    
    #define N
    n = 0
    if 12<p<49:
        n = 12
    elif 48<p<67:
        n = 9
    #define S
    s = 0
    if p<37:
        s = 12
    elif 39<p<67:
        s = 9
    #define E, W

    w, e = 1, 1 
    w_zero = [1,13,25,37,49,58,67]
    if p in w_zero:
        w = 0
    if p ==75 or p ==12 or p in b_e:
        e = 0

    #define value b
    b = 0
    if p>=2 and p <=11:
        b = -((Qp)*deltaX)/k
    elif p==13 or p==25:
        b = -(h*deltaY*ti)
    elif p==1:
        b = -Qp*deltaX/2 - (h *deltaX * ti)/2
    elif p==12:
        b = -((Qp*deltaX)/(k*2))
    elif p==37:
        b = -h *deltaY * ti/2
    elif p>=67 and p<=75:
        b = 25 + 273
    
    #Value P
    value_p = -4

    if p>=2 and p <=11:
        value_p = -2
    elif p in b_e:
        value_p = -2
    #verify 13,25
    elif p==13 or p==25:
        value_p = -2*k - h*deltaY
    elif p==38 or p==39:
        value_p = -2
    elif p==49 or p==58:
        value_p = -2
    elif p==1 or p==37:
        value_p = -k -(h *deltaY)/2
    elif p==12:
        value_p = -1
    elif p>66:
        value_p = 1

    #Value N
    value_n = 1

    if p>=2 and p <=11:
        value_n = 0
    elif p in b_e:
        value_n = 1/2
    elif p==13 or p==25:
        value_n = k/2
    elif p==38 or p==39:
        value_n = 1
    elif p==49 or p==58:
        value_n = 1/2
    elif p==1 :
        value_n = 0
    elif p==12:
        value_n = 0
    elif p==40:
        value_n = 1
    elif p==37:
        value_n = k/2
    elif p>66:
        value_n = 0
    #define S
    #Value S
    value_s = 1

    if p>=2 and p <=11:
        value_s = 1
    elif p in b_e:
        value_s = 1/2
    elif p==13 or p==25:
        value_s = k/2
    elif p==38 or p==39:
        value_s = 0
    elif p==49 or p==58:
        value_s = 1/2
    elif p==1 :
        value_s = k/2
    elif p==12:
        value_s = 1/2
    elif p==40:
        value_s = 1/2
    elif p==37:
        value_s = 0
    elif p>66:
        value_s = 0
    #define E
    #Value E
    value_e = 1

    if p>=2 and p <=11:
        value_e = 1/2
    elif p in b_e:
        value_e = 0
    elif p==13 or p==25:
        value_e = k
    elif p==38 or p==39:
        value_e = 1/2
    elif p==49 or p==58:
        value_e = 1
    elif p==1:
        value_e = k/2
    elif p==12:
        value_e = 0
    elif p==40:
        value_e = 1
    elif p==37:
        value_e = k/2
    elif p>66:
        value_e = 0
    #define W
    #Value W
    value_w = 1

    if p>=2 and p <=11:
        value_w = 1/2
    elif p in b_e:
        value_w = 1
    elif p==13 or p==25:
        value_w = 0
    elif p==38 or p==39:
        value_w = 1/2
    elif p==49 or p==58:
        value_w = 0
    elif p==1:
        value_w = 0
    elif p==12:
        value_w = 1/2
    elif p==40:
        value_w = 1/2
    elif p==37:
        value_w = 0
    elif p>66:
        value_w = 0    

    #at values
    eq[pn-n] = value_n
    eq[pn+s] = value_s
    eq[pn+e] = value_e
    eq[pn-w] = value_w
    eq[pn] = value_p

    return eq, b

def calc_trans(p, deltat, tp):
    
    #add points
    p_b = [30,31,32,42,43,44,51,52,53]
    
    if p in p_b:
        k = 20
    else:
        k = 15

    #const
    Qp,h,ti = 2000, 40, 25+273
    #atualizar const
    ro, cp = 8055, 480
    b_e = [24,36,48,57,66]
    deltaX, deltaY = 0.5,0.5
    pn = p-1
    eq = np.zeros((75,))
    
    #define N
    n = 0
    if 12<p<49:
        n = 12
    elif 48<p<67:
        n = 9
    #define S
    s = 0
    if p<37:
        s = 12
    elif 39<p<67:
        s = 9
    #define E, W
    w, e = 1, 1 
    w_zero = [1,13,25,37,49,58,67]
    if p in w_zero:
        w = 0
    if p ==75 or p ==12 or p in b_e:
        e = 0

    #define value b
    b = (ro * cp *deltaX*deltaY * tp)/ (deltat*k)
    if p>=2 and p <=11:
        b = -((Qp)*deltaX) + (ro * cp *deltaX*deltaY * tp)/ (2*deltat)
    elif p in b_e:
        b = b/2
    elif p==13 or p==25:
        b = ((ro * cp *deltaX*deltaY * tp)/ (2*deltat))  - h*deltaY*ti
    elif p==38 or p==39:
        b = b/2
    elif p==49 or p==58:
        b = b/2
    elif p==1:
        b = ((ro * cp *deltaX*deltaY * tp)/ (4*deltat)) -Qp*deltaX/2 - (h *deltaX * ti)/2
    elif p==12:
        b = ((ro * cp *deltaX*deltaY * tp)/ (4*deltat)) - h * deltaX *ti/2
    elif p==37:
        b = ((ro * cp *deltaX*deltaY * tp)/ (4*deltat)) - h * deltaX *ti/2
    elif p==40:
        b = ((3*ro * cp *deltaX*deltaY * tp)/ (4*deltat*k)) - h * deltaX *ti/2
    #duvida
    elif p>=67 and p<=75:
        b = 25 + 273
    
    #Value P
    value_p = -4 + (ro * cp *deltaX*deltaY)/(deltat*k)

    if p>=2 and p <=11:
        value_p = -2*k + (ro * cp *deltaX*deltaY)/(deltat*2)
    elif p in b_e:
        value_p = -2 + (ro * cp *deltaX*deltaY)/(deltat*2*k)
    #verify 13,25
    elif p==13 or p==25:
        value_p = -2*k - h*deltaY + (ro * cp *deltaX*deltaY)/(deltat*2)
    elif p==38 or p==39:
        value_p = -2 + (ro * cp *deltaX*deltaY)/(deltat*2*k)
    elif p==49 or p==58:
        value_p = -2 + (ro * cp *deltaX*deltaY)/(deltat*2*k)
    elif p==40:
        value_p = -3 + (3*ro * cp *deltaX*deltaY)/(deltat*4*k)
    elif p==1 or p==37:
        value_p = -k -(h *deltaY)/2 + (ro * cp *deltaX*deltaY)/(4)
    elif p==12:
        value_p = -k + (ro * cp *deltaX*deltaY)/(deltat*4)
    elif p>66:
        value_p = 1

    #Value N
    value_n = 1

    if p>=2 and p <=11:
        value_n = 0
    elif p in b_e:
        value_n = 1/2
    elif p==13 or p==25:
        value_n = k/2
    elif p==38 or p==39:
        value_n = 1
    elif p==49 or p==58:
        value_n = 1/2
    elif p==1 :
        value_n = 0
    elif p==12:
        value_n = 0
    elif p==40:
        value_n = 1
    elif p==37:
        value_n = k/2
    elif p>66:
        value_n = 0
    #define S
    #Value S
    value_s = 1

    if p>=2 and p <=11:
        value_s = 1
    elif p in b_e:
        value_s = 1/2
    elif p==13 or p==25:
        value_s = k/2
    elif p==38 or p==39:
        value_s = 0
    elif p==49 or p==58:
        value_s = 1/2
    elif p==1 :
        value_s = k/2
    elif p==12:
        value_s = 1/2
    elif p==40:
        value_s = 1/2
    elif p==37:
        value_s = 0
    elif p>66:
        value_s = 0
    #define E
    #Value E
    value_e = 1

    if p>=2 and p <=11:
        value_e = 1/2
    elif p in b_e:
        value_e = 0
    elif p==13 or p==25:
        value_e = k
    elif p==38 or p==39:
        value_e = 1/2
    elif p==49 or p==58:
        value_e = 1
    elif p==1:
        value_e = k/2
    elif p==12:
        value_e = 0
    elif p==40:
        value_e = 1
    elif p==37:
        value_e = k/2
    elif p>66:
        value_e = 0
    #define W
    #Value W
    value_w = 1

    if p>=2 and p <=11:
        value_w = 1/2
    elif p in b_e:
        value_w = 1
    elif p==13 or p==25:
        value_w = 0
    elif p==38 or p==39:
        value_w = 1/2
    elif p==49 or p==58:
        value_w = 0
    elif p==1:
        value_w = 0
    elif p==12:
        value_w = 1/2
    elif p==40:
        value_w = 1/2
    elif p==37:
        value_w = 0
    elif p>66:
        value_w = 0    

    #att values
    eq[pn-n] = value_n
    eq[pn+s] = value_s
    eq[pn+e] = value_e
    eq[pn-w] = value_w
    eq[pn] = value_p

    return eq, b

def plot_matrix(x):
    zeros = np.zeros((3,))
    x_plot = np.insert(x, 48,zeros)
    x_plot = np.insert(x_plot, 60,zeros)
    x_plot = np.insert(x_plot, 72,zeros)
    x_plot = x_plot.reshape((7,12))
    plt.imshow(x_plot, cmap='hot', interpolation='nearest')
    plt.show()


#calc perma
A, B = [], []

for i in range(1,76):
    a,b = calc_perm(i)
    A.append(a)
    B.append(b)
    

#calc X=A-1 B
A = np.asarray(A)
B = np.asarray(B)
print(A.shape)
print(B.shape)

x = np.linalg.solve(A,B)

for z,i in enumerate(x):
    print('T{} = {}'.format(z+1,i))

#plot Heatmap permanente
#plot_matrix(x)


#calc transiente
At, Bt = [], []
Tp = np.full((75,), 1000 + 273)#inicializar com 1000 C
deltaT = 1000
erro_p = 10
erro = 10000
j = 0
value_print = 3
while erro > erro_p:
    for i in range(1,76):
        #alterar a função
        a,b = calc_trans(i, deltaT, Tp[i-1])
        At.append(a)
        Bt.append(b)
    At = np.asarray(At)
    Bt = np.asarray(Bt)
    #calc new temp
    xt = np.linalg.solve(At,Bt)
    #erro
    erro = np.amax(xt - x)
    Tp = copy.copy(xt)
    j += 1
    if j%value_print == 0:
        plot_matrix(xt)
        for z,i in enumerate(Tp):
            print('Tp{} = {}'.format(z+1,i))
    At, Bt = [], []

for z,i in enumerate(Tp):
    print('Tp{} = {}'.format(z+1,i))


